import { initializeApp } from "firebase/app";
import { getFirestore} from "firebase/firestore";
// Iniciando firebase
const app = initializeApp({
  apiKey: "AIzaSyA665wi1yGF0fevGFKJO8vlI5AHcO4T84o",
  authDomain: "findparty-57ee1.firebaseapp.com",
  projectId: "findparty-57ee1",
  storageBucket: "findparty-57ee1.appspot.com",
  messagingSenderId: "1099077448265",
  appId: "1:1099077448265:web:d9beb488f2c01e28308598",
  measurementId: "G-PPM8NB1XKV",
});
export const db = getFirestore(app);

export default app;
