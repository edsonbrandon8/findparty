import React, { useState } from "react";
import {
  View,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
  LogBox,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import styles from "./Styles";

import { getFirestore, collection, addDoc } from "firebase/firestore";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { db } from "../../config/firebaseconfig";

import Logo from "../../Components/Logo/Logo";
import HeaderBanner from "../../Components/HeaderBanner/HeaderBanner";
import Terms from "../../Components/Terms/Terms";
import HandleTextInput from "../../Components/HandleTextInput/HandleTextInput";
import ButtonLogin from "../../Components/ButtonLogin/ButtonLogin";
import BouncyCheckbox from "react-native-bouncy-checkbox";

LogBox.ignoreLogs([
  "Warning: Async Storage has been extracted from react-native core",
]);
const Register = () => {
  // -- PEGANDO A ENTRADA DO DO USUÁRIO
  const [userName, setUserName] = useState(null);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);
  const [checkBoxState, setCheckboxState] = useState(false);
  const navigation = useNavigation();
  // -- FUNÇÃO DE VALIDAÇÃO DE VALIDAÇÃO DO FORMULÁRIO E CADASTRO DE NOVO USUÁRIO
  const handleNewAcount = () => {
    if (
      userName != null &&
      name != null &&
      email != null &&
      password != null &&
      confirmPassword != null
    ) {
      if (password === confirmPassword) {
        if (checkBoxState == true) {
          const auth = getAuth();
          createUserWithEmailAndPassword(auth, email, password, userName, name)
            .then(() => {
              const user = auth.currentUser;
              const uid = user.uid;
              console.log(uid);

              addDoc(collection(db, "users"), {
                name: name,
                username: userName,
                userid: uid,
              });

              alert("Cadastro feito com sucesso!");

              navigation.navigate("SingIn");
            })
            .catch((error) => {
              const errorCode = error.code;
              const errorMessage = error.message;

              console.log(errorCode);
              console.log(errorMessage);
            });
        } else {
          alert("Por favor aceite os termos!");
        }
      } else {
        alert("As senhas não são iguais");
      }
    } else {
      alert("Por favor, prencha todos os campos!");
    }
  };

  return (
    <SafeAreaView>
      <KeyboardAvoidingView keyboardVerticalOffset={80}>
        <ScrollView alwaysBounceVertical={true}>
          <View style={styles.container}>
            <HeaderBanner />

            <Logo color="black" fontSize={50} />

            <HandleTextInput
              title="Nome de usuário"
              onChangeText={(value) => setUserName(value)}
            />
            <HandleTextInput
              title="Nome"
              onChangeText={(value) => setName(value)}
            />
            <HandleTextInput
              title="E-mail"
              onChangeText={(value) => setEmail(value)}
            />
            <HandleTextInput
              title="Senha"
              password={true}
              onChangeText={(value) => setPassword(value)}
            />
            <HandleTextInput
              title="Confirmar Senha"
              password={true}
              onChangeText={(value) => setConfirmPassword(value)}
            />

            <ButtonLogin text="Cadastrar" onPress={handleNewAcount} />
            <View style={styles.waringContainer}>
              <BouncyCheckbox
                onPress={() => setCheckboxState(!checkBoxState)}
                fillColor="blue"
                unfillColor="#ffffff"
                iconStyle={{
                  borderRadius: 2,
                  height: 10,
                  width: 10,
                  marginBottom: 40,
                  marginLeft: 10,
                }}
              />
              <Terms />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default Register;
