import React from 'react';
import {ScrollView, View, Text, StatusBar, Image} from 'react-native';
import RadioButton from '../../Components/RadioButton';
import styles from './styles'
import TopBar from '../../Components/TopBar';
import EventBanner from '../../Components/EventBanner/EventBanner';
import EventStatus from '../../Components/EventStatus';
import Feed from '../../Components/Feed/Feed';

const EventProfile = () =>{

    return(
        <ScrollView style={styles.container}>
            <View style={styles.HeaderContainer}>
                <StatusBar
                    hidden = {true}
                />
                <TopBar text={'Evento'}/>
                <EventBanner/>
                <EventStatus/>
                
                <View style={styles.EventInfo}>
                <Text style={styles.EventName}>
                        Quintal Do Farofão
                    </Text>
                    <View style={styles.EventRating}>
                    
                        <Image source={require('../../../assets/star-icon.png')}>
                        
                        </Image>
                        <Text style={styles.textRating}>    
                            4,5
                        </Text>
                    </View>
                    
                </View>
                <View style={styles.ButtonsContainer}>
                    <RadioButton text='Avaliações'/>
                    <RadioButton text='Fotos'/>
                    <RadioButton text='Sobre'/>
                </View>
            </View>
            <View style={styles.eventAbout}>
                <Text style={styles.aboutTitle}>
                    Sobre
                </Text>
                <Text style={styles.textAbout}>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy 
                    text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to 
                    make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                </Text>
                <View style={styles.eventInfo}>
                    <Text style={styles.textInfo}>
                        Sex a Dom
                    </Text>
                    <Text style={styles.textInfo}>
                        19;00 as 03;00
                    </Text>
                    <Text style={styles.textInfo}>
                        Estr. Iaraquã, 525 - Campo Grande, Rio de Janeiro - RJ
                    </Text>
                </View>
            </View>
            <Text style={styles.pubTitle}>
                Publicações
            </Text>
            <Feed/>
            <Feed/>
            <Feed/>
        </ScrollView>
     
    )
}

export default EventProfile;