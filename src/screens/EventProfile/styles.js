import { Poppins_300Light, Poppins_600SemiBold } from "@expo-google-fonts/poppins";
import { Ranga_400Regular } from "@expo-google-fonts/ranga";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        display: 'flex',
        flexDirection: 'column'
    },
    HeaderContainer: {
        display: 'flex',
    },
    EventName:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24
    },
    EventInfo:{
        alignItems: 'center',
        marginTop: 5
    },
    EventRating:{
        display: 'flex',
        flexDirection: 'row'
    },
    textRating:{
        marginLeft: 8,
        fontFamily: 'Poppins_600SemiBold'
    },
    ButtonsContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10
    },
    eventAbout:{
        marginTop: 20
    },
    aboutTitle:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        textAlign: 'center'
    },
    textAbout:{
        fontFamily: 'Poppins_400Regular',
        fontSize: 12,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    eventInfo:{
        display: 'flex',
        justifyContent: 'flex-start',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20
    },
    textInfo: {
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 10,
        textAlign: 'left'
    },
    pubTitle:{
        fontFamily: 'Poppins_600SemiBold',
        fontSize: 24,
        textAlign: 'left',
        marginLeft: 10
    }
})

export default styles;