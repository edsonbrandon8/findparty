import React, {useState, useEffect} from 'react';
import { SafeAreaView, View, Text, ScrollView } from 'react-native';
import styles from './Styles';
import { useNavigation } from '@react-navigation/native';
import * as Location from 'expo-location';
import db from '../../config/firebaseconfig';
import { collection, getDocs } from 'firebase/firestore';

import ListEvents from '../../Components/ListEvents/ListEvents';
import PrefEvents from '../../Components/PrefEvents/PrefEvents';
import Feed from '../../Components/Feed/Feed';
import Menu from '../../Components/Menu/Menu';

const Home = () => {

  //pegando localização do usuário
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect (() =>{
    (async () =>{
      let {status} = await Location.requestForegroundPermissionsAsync();
      if(status != 'granted'){
        setErrorMsg('Permissão de acesso a localização negada');
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);

    })();
  }, []);

  let text =  'waiting';
  if(errorMsg) {
    text = errorMsg;
    console.log(text);

  }else if(location) {
    text = (location);
    var userLatitude = text.coords.latitude;
    var userLongitude = text.coords.longitude;

    console.log('latitude: ' + userLatitude);
    console.log('longitude: ' + userLongitude);

    
  }

  function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) { //calculando a distancia entre dois pontos
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }
  
  function deg2rad(deg) {
    return deg * (Math.PI/180)
  }
  
 const distance = getDistanceFromLatLonInKm(userLatitude, userLongitude, -22.8765336,-43.5405413);
 console.log('Distância: ' + distance);
  
  const navigation = useNavigation();
  
  const goToProfile = () =>{
    navigation.navigate('EventProfile');
  }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.container}>
                
                <ListEvents onPress={goToProfile}/>
                <PrefEvents />
                <Text style={styles.titleFeed}>Feed</Text>
                <Feed />
                <Feed />
                <Feed />
                <Feed />

            </ScrollView>

            <Menu />
        </SafeAreaView>
    );
};
export default Home;