import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  ImageBackground,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  LogBox
} from "react-native";

import { 
  getAuth,
   signInWithEmailAndPassword } 
from "firebase/auth";

import { useNavigation } from "@react-navigation/native";
import ButtonLogin from "../../Components/ButtonLogin/ButtonLogin";
import Logo from "../../Components/Logo/Logo";
import styles from "./Styles";

LogBox.ignoreLogs([
  "Warning: Async Storage has been extracted from react-native core",
]);

const image = {
  uri: "https://images.unsplash.com/photo-1516450360452-9312f5e86fc7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
};

const SingIn = () => {
  // --CAPTURANDO ENTRADAS DO USUÁRIO
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showElement, setShowElement] = useState(false);

  const navigation = useNavigation();

  const handleLogin = () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;
      console.log('Logado!');
      navigation.navigate('Home');
    }).catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;

      setShowElement(true);

      console.log(errorCode);
      console.log(errorMessage);
    });
  };

  const handleRegister = () => {
    navigation.navigate("Register");
  };
  const handleForgotPassword = () => {
    navigation.navigate("ForgotPassword");
  };

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={image} style={styles.backgroundStyle}>
        <Logo color="#FFFFFF" fontSize={200} />
        <KeyboardAvoidingView keyboardVerticalOffset={80}>
          <ScrollView>
            <View style={styles.formLogin}>
              <Text style={styles.labelLogin}>E-mail</Text>

              <TextInput
                style={styles.inputLogin}
                value={email}
                onChangeText={(value) => setEmail(value)}
                returnKeyType={"next"}
              ></TextInput>

              <Text style={styles.labelLogin}>Senha</Text>

              <TextInput
                style={styles.inputLogin}
                value={password}
                onChangeText={(value) => setPassword(value)}
                secureTextEntry={true}
                icon={<Text>Show</Text>}
                iconPosition="right"
              ></TextInput>

              <Text
                style={styles.forwardPassword}
                onPress={handleForgotPassword}
              >
                Esqueceu sua senha?
              </Text>
              {showElement ? (
                <Text style={styles.forwardPassword}>
                  {" "}
                  Usuário e/ou senha incorretos{" "}
                </Text>
              ) : null}
            </View>

            <View style={styles.buttonsView}>
              <ButtonLogin text="Entrar" onPress={handleLogin} />

              <Text style={styles.textRegister}>
                Não tem uma conta?
                <Text style={styles.registerClick} onPress={handleRegister}>
                  Registre-se!
                </Text>
              </Text>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default SingIn;
