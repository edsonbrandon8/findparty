import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    textContainer:{
        fontFamily: 'Poppins_600SemiBold',
        marginTop: 10,
        marginBottom: 10,
    }
});

export default styles;