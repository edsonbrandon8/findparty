import React, { useState } from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView } from 'react-native';

import { getAuth, sendPasswordResetEmail } from 'firebase/auth';

import Logo from '../../Components/Logo/Logo';
import HandleTextInput from '../../Components/HandleTextInput/HandleTextInput';
import ButtonLogin from '../../Components/ButtonLogin/ButtonLogin';

import styles from './Styles';

const ForgotPassword = () => {
    const [email, setEmail] = useState();

    const ForgotPassword = () => {
        const auth = getAuth();
        sendPasswordResetEmail(auth, email)
            .then(() => {
                alert('E-mail de redefinição enviado!');
            }).catch((error) => {
                alert('Esse e-mail não está cadastrado!');
                const errorCode = error.code;
                const errorMessage = error.message;
            });
    }

    return (

        <KeyboardAvoidingView keyboardVerticalOffset={80}>
            <ScrollView alwaysBounceVertical={true}>
                <View style={styles.container}>
                    <Logo fontSize={288} />
                    <Text style={styles.textContainer}>Vamos te enviar um e-mail para alterar a sua senha</Text>

                    <HandleTextInput
                        title='E-mail'
                        onChangeText={value => setEmail(value)}
                        style={styles.inputContainer}
                    />

                    <ButtonLogin text='Enviar' onPress={ForgotPassword} />
                </View>

            </ScrollView>
        </KeyboardAvoidingView>

    )
};

export default ForgotPassword;