import React from 'react';
import { View, ImageBackground } from 'react-native';
import styles from './styles';
import UserAvatar from '../../Components/UserAvatar';

const image = {
    uri: "https://images.unsplash.com/photo-1516450360452-9312f5e86fc7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
};

const EventBanner = () =>{

    return(
        <View>
            <ImageBackground source={image} style={styles.backgroundStyle} resizeMode='cover'>
                <UserAvatar styles={styles.avatar}/>
            </ImageBackground>
        </View>
        
    )
}

export default EventBanner;