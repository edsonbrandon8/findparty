import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    backgroundStyle:{
        width: '100%',
        height: 150,
        alignItems: 'center',
    },
    avatar:{
        position: 'absolute',
        marginTop: 50
    }
})

export default styles;