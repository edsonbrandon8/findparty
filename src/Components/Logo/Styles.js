import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    textStyle:{
        textAlign: 'center',
        fontFamily: 'Ranga_400Regular',
        textShadowColor: 'rgba(0,0,0,0.75)',
        textShadowOffset: {width: -1, height:1 },
        textShadowRadius: 10
    },
});

export default styles;