import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from './styles';

const backButton = () =>{
    
};

const TopBar = ({text}) =>{
    return (
        <View style={styles.container}>
            <TouchableOpacity>
                <Image source={require('../../../assets/arrow-back.png')} />
            </TouchableOpacity>
            
            <Text style={styles.text}>
                {text}
            </Text>
        </View>
    )
}

export default TopBar;