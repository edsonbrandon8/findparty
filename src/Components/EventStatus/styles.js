import { Poppins_600SemiBold } from "@expo-google-fonts/poppins";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    space:{
        marginTop: 10,
        height: 40,
        width: '100%',
        alignItems: 'center'
    },
    container:{
        borderRadius: 20,
        backgroundColor: '#0f2e',
        width: 100,
        height: 40,
        justifyContent: 'center'
    },
    TextStatus:{
        fontFamily: 'Poppins_600SemiBold',
        color: '#000',
        textAlign: 'center'
    }
})

export default styles;