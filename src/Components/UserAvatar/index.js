import React from 'react';
import {View, Image} from 'react-native';
import styles from './styles';

const UserAvatar = () =>{
    return(
        <View style={styles.container}>
            <Image style={styles.img} source={require('../../../assets/farofao.png')}></Image>
        </View>
    )
}

export default UserAvatar;