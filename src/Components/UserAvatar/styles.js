import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container:{
        alignItems: 'center',
        borderRadius: 100,
        width: 150,
        height: 150,
        marginTop: 30,
    },
    img:{
        borderRadius: 100
    }
})

export default styles;