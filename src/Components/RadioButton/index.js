import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

const RadioButton = ({text}) =>{

    return(
        <TouchableOpacity style={styles.container}>
            <Text style={styles.text}>
                {text}
            </Text>
        </TouchableOpacity>
    )
}

export default RadioButton;