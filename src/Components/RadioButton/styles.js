import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        width: 100,
        height: 30,
        borderRadius: 20,
        textAlign: 'center',
        display: 'flex',
        alignItems: "center",
        justifyContent: 'space-around'
    },
    text: {
        fontSize: 15,
        color: '#FFF',
        
    }
});

export default styles;