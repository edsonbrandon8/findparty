import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SingIn from '../screens/SingIn/SingIn';
import Home from '../screens/Home/Home';
import Register from '../screens/Register/Register';
import ForgotPassword from '../screens/ForgotPassword/ForgotPassword';
import EventProfile from '../screens/EventProfile/index';

const Stack = createNativeStackNavigator();

const AuthRoutes = () =>{
    return(
     
        <Stack.Navigator 
        screenOptions={{
            headerShown: false
          }}>
            <Stack.Screen
                name='Home'
                component={Home}
                
            />
            <Stack.Screen 
                name='SingIn'
                component={SingIn}
            />
            <Stack.Screen 
                name='Register'
                component={Register}
            />
            <Stack.Screen
                name='ForgotPassword'
                component={ForgotPassword}
            />
            <Stack.Screen
                name='EventProfile'
                component={EventProfile}
            />
        </Stack.Navigator>
       
       
    );
};
export default AuthRoutes;  