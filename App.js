import React, {useState, useEffect} from 'react';
import { StatusBar } from 'react-native';
import {useFonts} from 'expo-font';
import AppLoading from 'expo-app-loading';
import {Ranga_400Regular} from '@expo-google-fonts/ranga';
import {Poppins_700Bold, Poppins_600SemiBold, Poppins_300Light_Italic, Poppins_400Regular} from '@expo-google-fonts/poppins';


import { Routes } from './src/routes';

export default function App() {
  // Carregando fontes
  const [fontsLoaded] = useFonts({
    Ranga_400Regular,
    Poppins_600SemiBold,
    Poppins_700Bold,
    Poppins_300Light_Italic,
    Poppins_400Regular
  });
  if(!fontsLoaded) {
    
    return <AppLoading/>
  }
    return (
        <Routes/>
    );
}
